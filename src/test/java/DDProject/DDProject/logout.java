package DDProject.DDProject;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Resources.Base;
import pageobjects.LandingPage;
import pageobjects.Loginpage;
import pageobjects.userlogoutdropdown;

public class logout extends Base {
	
	public WebDriver driver;
	public  Logger log = LogManager.getLogger(Base.class.getName());
	//public WebDriver driver;
@BeforeTest	
	public void initializer() throws IOException {
		driver = initializeDriver();
		
	}

@Test
public void logoutpage() {
	driver.get(prop.getProperty("url"));
	LandingPage l=new LandingPage(driver);	
	l.getdropdownsignin().click();
	l.getsignin().click();
	log.info("sign in is done");
	
	
	
    Loginpage lp=new Loginpage(driver);
	lp.getemail().sendKeys("srikanthtesting100@gmail.com");
	lp.getpassword().sendKeys("password123");
	lp.getsubmit().click();
	log.info("login is done");
	
	userlogoutdropdown ldd=new userlogoutdropdown(driver);
	ldd.getuserlogoutdropdown().click();
	ldd.getLogout().click();
}

	@AfterTest
	public void teardown() {
		driver.close();
	}
	
	
}


