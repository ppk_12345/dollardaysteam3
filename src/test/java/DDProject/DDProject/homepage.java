package DDProject.DDProject;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Resources.Base;
import pageobjects.LandingPage;
import pageobjects.Loginpage;
import pageobjects.NewAddressBook;
import pageobjects.Receivingtype;
import pageobjects.myaddressbook;
import pageobjects.userlogindropdown;

	public class homepage extends Base {
		public WebDriver driver;
		public  Logger log = LogManager.getLogger(Base.class.getName());
		//public WebDriver driver;
	@Parameters
	@BeforeTest	
		public void initializer() throws IOException {
			driver = initializeDriver();
			
		}
	
	@Test(dataProvider = "getData")
	public void dollardayspage (String Firstname,String Lastname,String companyname,String Streetaddress,String apt,String country,String city,String state,String phonenumber,String ext,String zipcode) throws IOException {
	driver.get(prop.getProperty("url"));
	LandingPage l=new LandingPage(driver);	
	l.getdropdownsignin().click();
	l.getsignin().click();
	log.info("sign in is done");
	
	
	
    Loginpage lp=new Loginpage(driver);
	lp.getemail().sendKeys("srikanthtesting100@gmail.com");
	lp.getpassword().sendKeys("password123");
	lp.getsubmit().click();
	log.info("login is done");
	
	
	userlogindropdown uld=new userlogindropdown(driver);
	uld.getuserlogindropdown().click();
	uld.getaddressbook().click();
	log.info("login dropdown done");
	
	myaddressbook mab=new myaddressbook(driver);
	mab.getmyaddressbook().click();
	log.info("clicked on myadressbook");
	
	
	NewAddressBook nb=new NewAddressBook(driver);
	nb.getFirstName().sendKeys(Firstname);
	nb.getLastName().sendKeys(Lastname);
	nb.getCompanyName().sendKeys(companyname);
	nb.getStreetAddress().sendKeys(Streetaddress);
	nb.getApt().sendKeys(apt);
	nb.getCountry().sendKeys(country);
	nb.getCity().sendKeys(city);
	nb.getState().sendKeys(state);
	nb.getPhoneNumber().sendKeys(phonenumber);
	nb.getExt().sendKeys(ext);
	nb.getZipCode().sendKeys(zipcode);
	
	Receivingtype rt=new Receivingtype(driver);
	rt.getReceivingtype().click();
	rt.getsavechanges().click();
	
	
	}
	
	@DataProvider
	public Object getData() {
		Object[][] data=new Object[1][11];
		data[0][0]="priya";
		data[0][1]="k";
		data[0][2]="Bank of America";
		data[0][3]="Crowne lake cir";
		data[0][4]="4406";
		data[0][5]="United States";
		data[0][6]="Richmond";
		data[0][7]="VA";
		data[0][8]="3345678990";
		data[0][9]="+1";
		data[0][10]="12345";
		return data;
		
	}
	
	
	
	
	@AfterTest
	public void teardown() {
		driver.close();
	}
	}




