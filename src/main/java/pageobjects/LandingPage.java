package pageobjects;

	import org.openqa.selenium.By;
	import org.openqa.selenium.WebDriver;
	import org.openqa.selenium.WebElement;

	public class LandingPage {

		public WebDriver driver;

		By dropdownsignin = By.xpath("//*[@id=\"aspnetForm\"]/header/div/div/div/div[3]/div/ul/li[1]/a");
		By signin = By.xpath("//*[@id=\"aspnetForm\"]/header/div/div/div/div[3]/div/ul/li[1]/ul/li[2]/a");

		public LandingPage (WebDriver driver) {
			this.driver = driver;

		}

		public WebElement getdropdownsignin() {
			return driver.findElement(dropdownsignin);
		}

		public WebElement getsignin() {
			return driver.findElement(signin);
		}
	}

