package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class userlogindropdown {

	
	
public WebDriver driver;
By userlogindropdown=By.xpath("//*[@id=\'aspnetForm\']/header/div/div/div/div[3]/div/ul/li[1]/a/span");
By addressbook=By.xpath("//*[@id=\'aspnetForm\']/header/div/div/div/div[3]/div/ul/li[1]/ul/li[7]/a");
public userlogindropdown(WebDriver driver) {
	// TODO Auto-generated constructor stub
 this.driver=driver;

}
public WebElement getuserlogindropdown()
{
	return driver.findElement(userlogindropdown);
}
public WebElement getaddressbook()
{
	return driver.findElement(addressbook);
}

}