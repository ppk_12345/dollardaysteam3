package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class userlogoutdropdown {
	
	
	public WebDriver driver;
	By userlogoutdropdown=By.xpath("//*[@id=\"aspnetForm\"]/header/div[2]/div/div/div[3]/div/ul/li[1]/a/span");
	By Logout=By.xpath("//*[@id=\"aspnetForm\"]/header/div[2]/div/div/div[3]/div/ul/li[1]/ul/li[11]/a");
	
	public userlogoutdropdown(WebDriver driver) {
		// TODO Auto-generated constructor stub
	 this.driver=driver;

	}	
	public WebElement getuserlogoutdropdown()
	{
		return driver.findElement(userlogoutdropdown);
	}
	public WebElement getLogout()
	{
		return driver.findElement(Logout);
	}
	

}
