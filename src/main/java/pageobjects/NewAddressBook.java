package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NewAddressBook {
	
	public WebDriver driver;
	
By FirstName=By.id("ctl00_cphContent_txtfirstname")	;
By LastName=By.id("ctl00_cphContent_txtlastname");
By CompanyName=By.id("ctl00_cphContent_txtAddrshipcompany");
By StreetAddress=By.id("ctl00_cphContent_txtAddrshipaddr1");
By Apt=By.id("ctl00_cphContent_txtAddrshipaddr2");
By Country=By.id("ctl00_cphContent_ddlcountry");
By City=By.id("ctl00_cphContent_txtAddrshipcity");
By State=By.id("ctl00_cphContent_ddlshipstate");
By PhoneNumber=By.id("ctl00_cphContent_txtAddrshipphone");
By Ext=By.id("ctl00_cphContent_txtAddrshipphoneExt");
By ZipCode=By.id("txtAddrshipzip");


public NewAddressBook(WebDriver driver) {
	 this.driver=driver;
}

public WebElement getFirstName()
{
	return driver.findElement(FirstName);
}

public WebElement getLastName()
{
	return driver.findElement(LastName);
}
public WebElement getCompanyName()
{
	return driver.findElement(CompanyName);
}
public WebElement getStreetAddress()
{
	return driver.findElement(StreetAddress);
}
public WebElement getApt()
{
	return driver.findElement(Apt);
}
public WebElement getCountry()
{
	return driver.findElement(Country);
}
public WebElement getCity()
{
	return driver.findElement(City);
}
public WebElement getState()
{
	return driver.findElement(State);
}
public WebElement getPhoneNumber()
{
	return driver.findElement(PhoneNumber);
}
public WebElement getExt()
{
	return driver.findElement(Ext);
}
public WebElement getZipCode()
{
	return driver.findElement(ZipCode);
}



}
