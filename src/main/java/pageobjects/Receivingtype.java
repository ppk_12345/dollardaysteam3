package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Receivingtype {
	public WebDriver driver;
	
	By Receivingtype=By.xpath("//*[@id=\"divTxtShipAddr\"]/div/div[12]/div/ul/li[3]/label");
	By savechanges=By.id("ctl00_cphContent_btnAddAddress");
	
	public Receivingtype (WebDriver driver) {
		this.driver = driver;

	}
	
	public WebElement getReceivingtype() {
		return driver.findElement(Receivingtype);
	}

	public WebElement getsavechanges() {
		return driver.findElement(savechanges);
	}

	
	
}
